# Command Line Shortcuts

[Procesess](#processes) | 
[Control the screen](#screen) |
[Moving the cursor](#cursor) |
[Deleting text](#delete) |
[Fixing Typos](#fixing) |
[Cut and Paste](#cut-paste) |
[Capitalize characters](#capitalize) |
[Tricks](#tricks)

## <a name="#processes">Working With Processes</a>

Use the following shortcuts to manage running processes.

- **<kbd>Ctr</kbd>** + **<kbd>C</kbd>** : Interrupt (kill) the current foreground process running in in the terminal. This sends the SIGINT signal to the process, which is technically just a request—most processes will honor it, but some may ignore it.

- **<kbd>Ctr</kbd>** + **<kbd>Z</kbd>**: Suspend the current foreground process running in bash. This sends the SIGTSTP signal to the process. To return the process to the foreground later, use the fg process_name command.
    
- **<kbd>Ctr</kbd>** + **<kbd>D</kbd>**: Close the bash shell. This sends an EOF (End-of-file) marker to bash, and bash exits when it receives this marker. This is similar to running the exit command.

## <a name="#screen">Controlling the Screen</a>

The following shortcuts allow you to control what appears on the screen.

- **<kbd>Ctr</kbd>** + **<kbd>L</kbd>**: Clear the screen. This is similar to running the “clear” command.
- **<kbd>Ctr</kbd>** + **<kbd>S</kbd>**: Stop all output to the screen. This is particularly useful when running commands with a lot of long, verbose output, but you don’t want to stop the command itself with Ctrl+C.
- **<kbd>Ctr</kbd>** + **<kbd>Q</kbd>**: Resume output to the screen after stopping it with Ctrl+S.

## <a name="#cursor">Moving the Cursor</a>

Use the following shortcuts to quickly move the cursor around the current line while typing a command.

- **<kbd>Ctr</kbd>** + **<kbd>A</kbd>** or **<kdb>Home</kbd>**: Go to the beginning of the line.
- **<kbd>Ctr</kbd>** + **<kbd>E</kbd>** or **<kbd>End</kbd>**: Go to the end of the line.
- **<kbd>Alt</kbd>** + **<kbd>B</kbd>**: Go left (back) one word.
- **<kbd>Ctr</kbd>** + **<kbd>B</kbd>**: Go left (back) one character.
- **<kbd>Alt</kbd>** + **<kbd>F</kbd>**: Go right (forward) one word.
- **<kbd>Ctr</kbd>** + **<kbd>F</kbd>**: Go right (forward) one character.
- **<kbd>Ctr</kbd>** + **<kbd>XX</kbd>**: Move between the beginning of the line and the current position of the cursor. This allows you to press `Ctrl+XX` to return to the start of the line, change something, and then press `Ctrl+XX` to go back to your original cursor position. To use this shortcut, hold the `Ctrl` key and tap the `X` key twice.

## <a name="#delete">Deleting Text</a>

Use the following shortcuts to quickly delete characters:

- **<kbd>Ctr</kbd>** + **<kbd>D</kbd>** or **<kbd>Delete</kbd>**: Delete the character under the cursor.
- **<kbd>Alt</kbd>** + **<kbd>D</kbd>**: Delete all characters after the cursor on the current line.
- **<kbd>Ctr</kbd>** + **<kbd>H</kbd>** or **<kbd>Backspace</kbd>**: Delete the character before the cursor.

## <a name="#fixing">Fixing Typos</a>

These shortcuts allow you to fix typos and undo your key presses.

- **<kbd>Alt</kbd>** + **<kbd>T</kbd>**: Swap the current word with the previous word.
- **<kbd>Ctr</kbd>** + **<kbd>T</kbd>**: Swap the last two characters before the cursor with each other. You can use this to quickly fix typos when you type two characters in the wrong order.
- **<kbd>Ctr</kbd>** + **<kbd>_</kbd>**: Undo your last key press. You can repeat this to undo multiple times.

## <a name="#cut-paste">Cutting and Pasting</a>

Bash includes some basic cut-and-paste features.

- **<kbd>Ctr</kbd>** + **<kbd>W</kbd>**: Cut the word before the cursor, adding it to the clipboard.
- **<kbd>Ctr</kbd>** + **<kbd>K</kbd>**: Cut the part of the line after the cursor, adding it to the clipboard.
- **<kbd>Ctr</kbd>** + **<kbd>U</kbd>**: Cut the part of the line before the cursor, adding it to the clipboard.
- **<kbd>Ctr</kbd>** + **<kbd>Y</kbd>**: Paste the last thing you cut from the clipboard. The y here stands for “yank”.

## <a name="#capitalize">Capitalizing Characters</a>

The bash shell can quickly convert characters to upper or lower case:

- **<kbd>Alt</kbd>** + **<kbd>U</kbd>**: Capitalize every character from the cursor to the end of the current word, converting the characters to upper case.
- **<kbd>Alt</kbd>** + **<kbd>L</kbd>**: Uncapitalize every character from the cursor to the end of the current word, converting the characters to lower case.
- **<kbd>Alt</kbd>** + **<kbd>C</kbd>**: Capitalize the character under the cursor. Your cursor will move to the end of the current word.

## <a name="#tricks">Tricks</a> 

**<kbd>Tab</kbd>** Completion

Tab completion is a very useful bash feature. While typing a file, directory, or command name, press Tab and bash will automatically complete what you’re typing, if possible. If not, bash will show you various possible matches and you can continue typing and pressing Tab to finish typing.


**Working With Your Command History**

You can quickly scroll through your recent commands, which are stored in your user account’s bash history file:

- **<kbd>Ctr</kbd>** + **<kbd>P</kbd>** or **<kbd>Up Arrow</kbd>**: Go to the previous command in the command history. Press the shortcut multiple times to walk back through the history.
- **<kbd>Ctr</kbd>** + **<kbd>N</kbd>** or **<kbd>Down Arrow</kbd>**:  Go to the next command in the command history. Press the shortcut multiple times to walk forward through the history.
- **<kbd>Alt</kbd>** + **<kbd>R</kbd>**: Revert any changes to a command you’ve pulled from your history if you’ve edited it.

Bash also has a special “recall” mode you can use to search for commands you’ve previously run:
- **<kbd>Ctr</kbd>** + **<kbd>R</kbd>**: Recall the last command matching the characters you provide. Press this shortcut and start typing to search your bash history for a command.
- **<kbd>Ctr</kbd>** + **<kbd>O</kbd>**:  Run a command you found with Ctrl+R.
- **<kbd>Ctr</kbd>** + **<kbd>G</kbd>**:  Leave history searching mode without running a command.

**emacs vs. vi Keyboard Shortcuts**

The above instructions assume you’re using the default keyboard shortcut configuration in bash. By default, bash uses emacs-style keys. If you’re more used to the vi text editor, you can switch to vi-style keyboard shortcuts.

The following command will put bash into vi mode:

```bash
set -o vi
```
The following command will put bash back into the default emacs mode:

```bash
set -o emacs
```

[Source](https://www.howtogeek.com/howto/ubuntu/keyboard-shortcuts-for-bash-command-shell-for-ubuntu-debian-suse-redhat-linux-etc/)