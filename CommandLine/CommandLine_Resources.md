# Command Line Resources

## Basics 
[The Art of the Command Line](https://github.com/jlevy/the-art-of-command-line)

[Write down a command-line to see the help text that matches each argument](https://explainshell.com/)

## Shell
[Oh my Zsh!](https://github.com/ohmyzsh/ohmyzsh)
